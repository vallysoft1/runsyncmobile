﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MvvmCross.Plugin.FieldBinding;
using MvvmCross.ViewModels;
using Newtonsoft.Json;
using RunSync.Core.Extensions;
using RunSync.Core.Infrastracture;
using Xamarin.Auth;
using Xamarin.Essentials;

namespace RunSync.Core.ViewModels
{
    public class SyncViewModel : MvxViewModel
    {
        public INC<bool> HasStrava = new NC<bool>(false);
        public INC<string> StravaToken = new NC<string>();

        private readonly MvxInteraction<CustomParametersOAuth2Authenticator> stravaInteraction = new MvxInteraction<CustomParametersOAuth2Authenticator>();
        public IMvxInteraction<CustomParametersOAuth2Authenticator> StravaUrlInteraction => stravaInteraction;

        public void OAuthStrava()
        {
            var parameters = new Dictionary<string, string> {{"response_type", "code"}, {"approval_prompt", "auto"}};
            var auth = new CustomParametersOAuth2Authenticator(
                clientId: "41107",
                clientSecret: "77153e12e82d5f8ee0c981f840dccb8d97fa7e2a",
                scope: "activity:write,read",
                authorizeUrl: new Uri("https://www.strava.com/oauth/mobile/authorize"),
                redirectUrl: new Uri("https://runsync.vallysoft.tech/oauth2redirect"),
                accessTokenUrl: new Uri("https://www.strava.com/oauth/token"), 
                customParameters: parameters, 
                isUsingNativeUI: true
                );

            auth.AllowCancel = false;
            
            auth.Completed += AuthStravaOnCompleted;
            auth.ShouldEncounterOnPageLoading = false;
            
            this.stravaInteraction.Raise(auth);
        }

        private void AuthStravaOnCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            if (!e.IsAuthenticated) return;
            
            this.HasStrava.Value = true;

            Task.Factory.StartNew(async () => await SecureStorage.SetAsync("strava-account", JsonConvert.SerializeObject(e.Account)).ConfigureAwait(false)).ConfigureAwait(false);

            Task.Factory.StartNew( async() =>
            {
                var account = await SecureStorageExt.GetAsync<Account>("strava-account");
                this.StravaToken.Value = account.Properties["access_token"];
            });
        }
    }
}
