﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Auth;

namespace RunSync.Core.Infrastracture
{
    public class CustomParametersOAuth2Authenticator : OAuth2Authenticator
    {
        private readonly Dictionary<string, string> customParameters;

        public CustomParametersOAuth2Authenticator(string clientId, string scope, Uri authorizeUrl, Uri redirectUrl, Dictionary<string, string> customParameters, GetUsernameAsyncFunc getUsernameAsync = null, bool isUsingNativeUI = false) 
            : base(clientId, scope, authorizeUrl, redirectUrl, getUsernameAsync, isUsingNativeUI)
        {
            this.customParameters = customParameters;
        }

        public CustomParametersOAuth2Authenticator(string clientId, string clientSecret, string scope, Uri authorizeUrl, Uri redirectUrl, Uri accessTokenUrl, Dictionary<string, string> customParameters, GetUsernameAsyncFunc getUsernameAsync = null, bool isUsingNativeUI = false) 
            : base(clientId, clientSecret, scope, authorizeUrl, redirectUrl, accessTokenUrl, getUsernameAsync, isUsingNativeUI)
        {
            this.customParameters = customParameters;
        }

        public override Task<Uri> GetInitialUrlAsync(Dictionary<string, string> query_parameters = null)
        {

            this.RequestParameters = CreateRequestQueryParameters(this.customParameters);

            OnCreatingInitialUrl(RequestParameters);

            var queryString = string.Join("&", RequestParameters.Select(i => i.Key + "=" + i.Value));

            var url = string.IsNullOrEmpty(queryString) ? this.AuthorizeUrl : new Uri(this.AuthorizeUrl.AbsoluteUri + "?" + queryString);

            return Task.FromResult(url);
        }
    }
}