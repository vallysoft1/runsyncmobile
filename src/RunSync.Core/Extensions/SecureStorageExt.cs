﻿using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Essentials;

namespace RunSync.Core.Extensions
{
    public static class SecureStorageExt
    {
        public static async Task<T> GetAsync<T>(string key)
        {
            var objStr = await SecureStorage.GetAsync(key).ConfigureAwait(false);
            return JsonConvert.DeserializeObject<T>(objStr);
        } 
    }
}