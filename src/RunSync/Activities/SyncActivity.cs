﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using MvvmCross.Base;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.ViewModels;
using Newtonsoft.Json;
using RunSync.Core.Infrastracture;
using RunSync.Core.ViewModels;
using Xamarin.Auth;

namespace RunSync.Android.Activities
{
    [MvxActivityPresentation]
    [Activity(Label = "RunSync",
        Theme = "@style/AppTheme",
        LaunchMode = LaunchMode.SingleTop,
        ClearTaskOnLaunch = true,
        AlwaysRetainTaskState = true
    )]
   
    public class SyncActivity : MvxAppCompatActivity<SyncViewModel>
    {
        public static CustomParametersOAuth2Authenticator StravaAuthenticator;

        private IMvxInteraction<CustomParametersOAuth2Authenticator> stravaInteraction;

        public IMvxInteraction<CustomParametersOAuth2Authenticator> StravaInteraction
        {
            get => stravaInteraction;
            set
            {
                if (stravaInteraction != null)
                    stravaInteraction.Requested -= OnStravaUrlInteractionRequested;

                stravaInteraction = value;
                stravaInteraction.Requested += OnStravaUrlInteractionRequested;
            }
        }

        private void OnStravaUrlInteractionRequested(object sender,
            MvxValueEventArgs<CustomParametersOAuth2Authenticator> e)
        {
            StravaAuthenticator = e.Value;

            var intent = StravaAuthenticator.GetUI(this);
            StartActivity(intent);
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_sync);

            var set = this.CreateBindingSet<SyncActivity, SyncViewModel>();
            set.Bind(this).For(activity => activity.StravaInteraction).To(viewModel => viewModel.StravaUrlInteraction)
                .OneWay();
            set.Apply();
        }
    }
}