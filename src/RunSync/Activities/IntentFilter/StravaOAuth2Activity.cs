﻿using System;
using Android.App;
using Android.Content;
using Android.OS;

namespace RunSync.Android.Activities.IntentFilter
{
    [Activity(Label = "StravaOAuth2")]
    [IntentFilter(
        new[] {Intent.ActionView},
        Categories = new[] {Intent.CategoryDefault, Intent.CategoryBrowsable},
        DataSchemes = new[] {"http", "https"},
        DataHosts = new[] { "runsync.vallysoft.tech" },
        DataPath = "/oauth2redirect")]
    public class StravaOAuth2Activity : Activity
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var s = Intent.Data?.ToString();

            if (s != null)
            {
                var uri = new Uri(s);

                SyncActivity.StravaAuthenticator.OnPageLoaded(uri);
            }

            this.Finish();
        }
    }
}